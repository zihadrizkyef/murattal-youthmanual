package com.zihadrizkyef.murattalyouthmanual

import android.support.v7.widget.RecyclerView
import android.view.View
import androidx.test.espresso.NoMatchingViewException
import androidx.test.espresso.ViewAssertion
import androidx.test.espresso.matcher.ViewMatchers.assertThat
import org.hamcrest.Matcher
import org.hamcrest.Matchers.`is`

class RecyclerViewMatcher(private var matcher: Matcher<Int>) : ViewAssertion {
    companion object {
        fun withItemCount(expectedCount: Int): RecyclerViewMatcher {
            return withItemCount(`is`(expectedCount))
        }

        fun withItemCount(matcher: Matcher<Int>): RecyclerViewMatcher {
            return RecyclerViewMatcher(matcher)
        }
    }

    override fun check(view: View, noViewFoundException: NoMatchingViewException?) {
        if (noViewFoundException != null) {
            throw noViewFoundException
        }

        val recyclerView = view as RecyclerView
        val adapter = recyclerView.adapter
        assertThat(adapter!!.itemCount, matcher)
    }
}