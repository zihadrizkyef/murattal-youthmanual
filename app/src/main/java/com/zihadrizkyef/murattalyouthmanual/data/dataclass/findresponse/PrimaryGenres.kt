package com.zihadrizkyef.murattalyouthmanual.data.dataclass.findresponse

data class PrimaryGenres(
    val music_genre_list: List<MusicGenre>
)