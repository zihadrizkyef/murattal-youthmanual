package com.zihadrizkyef.murattalyouthmanual.data.dataclass.findresponse

data class Body(
    val track_list: List<Track>
)