package com.zihadrizkyef.murattalyouthmanual.data.dataclass

import io.realm.RealmObject

open class CalendarEvent(
        var day: Int = 0,
        var month: Int = 0,
        var year: Int = 0,
        var hour: Int = 0,
        var minute: Int = 0,
        var name: String = "",
        var descriction: String = ""
): RealmObject()