package com.zihadrizkyef.murattalyouthmanual.data.local

import android.content.Context
import android.content.SharedPreferences

class SharedPref(context: Context) {
    companion object {
        private const val PREF_NAME = "murattalyouthmanual"
        private const val KEY_EMAIL = "email"
    }

    private val INSTANCE: SharedPreferences by lazy {
        context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
    }

    var email: String
        get() = INSTANCE.getString(KEY_EMAIL, "")!!
        set(value) {INSTANCE.edit().putString(KEY_EMAIL, value).apply()}
}