package com.zihadrizkyef.murattalyouthmanual.data.dataclass.findresponse

import io.realm.RealmObject
import io.realm.annotations.Ignore
import io.realm.annotations.RealmClass

open class TrackX(
    var album_id: Int = 0,
    var album_name: String = "",
    var artist_id: Int = 0,
    var artist_name: String = "",
    var commontrack_id: Int = 0,
    var explicit: Int = 0,
    var has_lyrics: Int = 0,
    var has_richsync: Int = 0,
    var has_subtitles: Int = 0,
    var instrumental: Int = 0,
    var num_favourite: Int = 0,
    @Ignore var primary_genres: PrimaryGenres? = null,
    var restricted: Int = 0,
    var track_edit_url: String = "",
    var track_id: Int = 0,
    var track_name: String = "",
    @Ignore var track_name_translation_list: List<Any>? = null,
    var track_rating: Int = 0,
    var track_share_url: String = "",
    var updated_time: String = ""
) : RealmObject()