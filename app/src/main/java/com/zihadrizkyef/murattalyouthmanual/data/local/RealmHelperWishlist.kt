package com.zihadrizkyef.murattalyouthmanual.data.local

import android.content.Context
import com.zihadrizkyef.murattalyouthmanual.data.dataclass.findresponse.TrackX
import io.realm.Realm

class RealmHelperWishlist(private val context: Context) {
    private val realm: Realm by lazy {
        Realm.init(context)
        Realm.getDefaultInstance()
    }

    fun addWish(track: TrackX) {
        realm.beginTransaction()
        realm.copyToRealm(track)
        realm.commitTransaction()
    }

    fun getWishlist(): ArrayList<TrackX> {
        val result = realm.where(TrackX::class.java).findAll()
        return if (result.isEmpty()) {
            arrayListOf()
        } else {
            val list = arrayListOf<TrackX>()
            list.addAll(result)
            list
        }
    }

    fun contains(track: TrackX): Boolean {
        val result = realm.where(TrackX::class.java)
            .equalTo("track_id", track.track_id)
            .equalTo("album_id", track.album_id)
            .equalTo("artist_id", track.artist_id)
            .findAll()

        return !result.isEmpty()
    }

    fun removeAllWishList(track: TrackX) {
        realm.beginTransaction()
        realm.deleteAll()
        realm.commitTransaction()
    }

    fun removeWish(track: TrackX) {
        val result = realm.where(TrackX::class.java)
            .equalTo("track_id", track.track_id)
            .equalTo("album_id", track.album_id)
            .equalTo("artist_id", track.artist_id)
            .findAll()

        realm.beginTransaction()
        result.deleteAllFromRealm()
        realm.commitTransaction()
    }

    fun close() {
        realm.close()
    }
}