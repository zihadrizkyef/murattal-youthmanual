package com.zihadrizkyef.murattalyouthmanual.data.dataclass.findresponse

data class ResponseBody(
    val message: Message
)