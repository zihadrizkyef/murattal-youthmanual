package com.zihadrizkyef.murattalyouthmanual.data.dataclass.findresponse

data class Header(
    val available: Int,
    val execute_time: Double,
    val status_code: Int
)