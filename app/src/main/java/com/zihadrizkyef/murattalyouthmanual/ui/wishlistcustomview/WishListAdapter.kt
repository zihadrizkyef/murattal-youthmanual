package com.zihadrizkyef.murattalyouthmanual.ui.wishlistcustomview

import android.annotation.SuppressLint
import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import com.zihadrizkyef.murattalyouthmanual.R
import com.zihadrizkyef.murattalyouthmanual.data.dataclass.findresponse.TrackX
import com.zihadrizkyef.murattalyouthmanual.data.local.RealmHelperWishlist

class WishListAdapter(private val context: Context, private var list: ArrayList<TrackX>) : RecyclerView.Adapter<WishListViewHolder>() {
    var onEmptyList = {}
    private val realmDb = RealmHelperWishlist(context)

    override fun onCreateViewHolder(parent: ViewGroup, type: Int): WishListViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.itemlist_wish, parent, false)
        return WishListViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: WishListViewHolder, position: Int) {
        val track = list[position]

        holder.tvId.text = "#${track.track_id} pos$position"
        holder.tvArtistName.text = track.artist_name
        holder.tvTrackName.text = track.track_name

        holder.btRemoveClickListener = {
            if (realmDb.contains(track)) {
                list.remove(track)
                realmDb.removeWish(track)

                if (realmDb.getWishlist().isEmpty()) {
                    onEmptyList()
                }

                notifyItemRemoved(position)
                notifyItemRangeChanged(position, itemCount)
            }
        }
    }
}

class WishListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    val tvId = itemView.findViewById<TextView>(R.id.tvId)
    val tvArtistName = itemView.findViewById<TextView>(R.id.tvDescription)
    val tvTrackName = itemView.findViewById<TextView>(R.id.tvName)
    val btRemove = itemView.findViewById<ImageButton>(R.id.btRemove)
    var btRemoveClickListener = {}

    init {
        btRemove.setOnClickListener { btRemoveClickListener() }
    }
}