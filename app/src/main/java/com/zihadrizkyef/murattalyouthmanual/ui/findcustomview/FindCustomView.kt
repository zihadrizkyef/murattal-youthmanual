package com.zihadrizkyef.murattalyouthmanual.ui.findcustomview

import android.content.Context
import android.support.design.widget.Snackbar
import android.support.v7.widget.LinearLayoutCompat
import android.support.v7.widget.LinearLayoutManager
import android.util.AttributeSet
import android.view.View
import com.zihadrizkyef.murattalyouthmanual.R
import com.zihadrizkyef.murattalyouthmanual.data.dataclass.findresponse.ResponseBody
import com.zihadrizkyef.murattalyouthmanual.data.dataclass.findresponse.Track
import com.zihadrizkyef.murattalyouthmanual.data.network.NetworkClient
import kotlinx.android.synthetic.main.customview_find.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class FindCustomView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0,
    private var idlingResource: FetchingDataListener? = null
) :
    LinearLayoutCompat(context, attrs, defStyleAttr) {

    private var adapter: FindAdapter
    private val list = arrayListOf<Track>()
    private val networkClient = NetworkClient().INSTANCE

    init {
        View.inflate(context, R.layout.customview_find, this)

        adapter = FindAdapter(context, list)
        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.adapter = adapter
        recyclerView.hasFixedSize()
    }

    override fun setVisibility(visibility: Int) {
        super.setVisibility(visibility)

        if (visibility == View.VISIBLE) {
            adapter.notifyDataSetChanged()
        }
    }

    fun searchTrack(query: String) {
        showProgressBar()
        networkClient.findMurattal(query).enqueue(object : Callback<ResponseBody> {
            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                t.printStackTrace()

                hideProgressBar()
                showSearchNoNetwork(query)
                idlingResource?.isFetchingData = false
            }

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                hideProgressBar()
                showTrack(response.body()!!.message.body.track_list)
                idlingResource?.isFetchingData = false
            }
        })
    }

    fun showTrack(trackList: List<Track>) {
        if (trackList.isEmpty()) {
            tvEmptyRv.visibility = View.VISIBLE
            recyclerView.visibility = View.GONE
        } else {
            tvEmptyRv.visibility = View.GONE
            recyclerView.visibility = View.VISIBLE
        }

        list.clear()
        list.addAll(trackList)
        adapter.notifyDataSetChanged()
    }

    private fun showSearchNoNetwork(query: String) {
        Snackbar.make(constraintLayout, "No network, please try again", Snackbar.LENGTH_LONG)
            .setAction("Try again") { searchTrack(query) }
            .show()
    }

    private fun showProgressBar() {
        progressBar.animate().scaleY(1F).scaleX(1F).start()
    }

    private fun hideProgressBar() {
        progressBar.animate().scaleY(0F).scaleX(0F).start()
    }
}
