package com.zihadrizkyef.murattalyouthmanual.ui.wishlistcustomview

import android.content.Context
import android.support.v7.widget.LinearLayoutCompat
import android.support.v7.widget.LinearLayoutManager
import android.util.AttributeSet
import android.view.View
import com.zihadrizkyef.murattalyouthmanual.R
import com.zihadrizkyef.murattalyouthmanual.data.dataclass.findresponse.TrackX
import com.zihadrizkyef.murattalyouthmanual.data.local.RealmHelperWishlist
import kotlinx.android.synthetic.main.customview_wishlist.view.*

class WishlistCustomView @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0)
    : LinearLayoutCompat(context, attrs, defStyleAttr) {

    private var adapter: WishListAdapter
    private val list = arrayListOf<TrackX>()
    private var realm : RealmHelperWishlist

    init {
        View.inflate(context, R.layout.customview_wishlist, this)

        realm = RealmHelperWishlist(context)

        adapter = WishListAdapter(context, list)
        adapter.onEmptyList = {
            recyclerView.visibility = View.GONE
            tvEmptyRv.visibility = View.VISIBLE
        }
        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.adapter = adapter
    }

    override fun setVisibility(visibility: Int) {
        super.setVisibility(visibility)
        if (visibility == View.VISIBLE) {
            showWishList()
        }
    }

    fun showWishList() {
        if (realm.getWishlist().isNotEmpty()) {
            list.clear()
            list.addAll(realm.getWishlist())
            adapter.notifyDataSetChanged()
            recyclerView.visibility = View.VISIBLE
            tvEmptyRv.visibility = View.GONE
        } else {
            recyclerView.visibility = View.GONE
            tvEmptyRv.visibility = View.VISIBLE
        }
    }
}