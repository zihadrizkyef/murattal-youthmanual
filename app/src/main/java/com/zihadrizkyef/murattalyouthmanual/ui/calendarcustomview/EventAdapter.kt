package com.zihadrizkyef.murattalyouthmanual.ui.calendarcustomview

import android.annotation.SuppressLint
import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.zihadrizkyef.murattalyouthmanual.data.dataclass.CalendarEvent
import com.zihadrizkyef.murattalyouthmanual.R

class EventAdapter(private val context: Context, private var list: ArrayList<CalendarEvent>) :
        RecyclerView.Adapter<EventViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, type: Int): EventViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.itemlist_event, parent, false)
        return EventViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: EventViewHolder, position: Int) {
        val event = list[position]

        holder.tvName.text = event.name
        holder.tvDescription.text = event.descriction
        holder.tvTime.text = "${event.day}/${event.month}/${event.year} ${event.hour}:${event.minute}"
    }
}

class EventViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    val tvDescription = itemView.findViewById<TextView>(R.id.tvDescription)
    val tvName = itemView.findViewById<TextView>(R.id.tvName)
    val tvTime = itemView.findViewById<TextView>(R.id.tvTime)
}