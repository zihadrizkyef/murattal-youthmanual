package com.zihadrizkyef.murattalyouthmanual.ui.findcustomview

import android.annotation.SuppressLint
import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import com.zihadrizkyef.murattalyouthmanual.R
import com.zihadrizkyef.murattalyouthmanual.data.dataclass.findresponse.Track
import com.zihadrizkyef.murattalyouthmanual.data.local.RealmHelperWishlist

class FindAdapter(private val context: Context, private var list: ArrayList<Track>) : RecyclerView.Adapter<FindViewHolder>() {
    private val wishlistRealm = RealmHelperWishlist(context)

    override fun onCreateViewHolder(parent: ViewGroup, type: Int): FindViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.itemlist_find, parent, false)
        return FindViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: FindViewHolder, position: Int) {
        val track = list[position]

        holder.tvId.text = "#${track.track.track_id}"
        holder.tvArtistName.text = track.track.artist_name
        holder.tvTrackName.text = track.track.track_name

        if (wishlistRealm.contains(track.track)) {
            holder.btWishList.setImageResource(R.drawable.ic_favorite_red)
        } else {
            holder.btWishList.setImageResource(R.drawable.ic_favorite_black)
        }

        holder.btWishListClickListener = {
            if (wishlistRealm.contains(track.track)) {
                wishlistRealm.removeWish(track.track)
                notifyItemChanged(position)
            } else {
                wishlistRealm.addWish(track.track)
                notifyItemChanged(position)
            }
        }
    }
}

class FindViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    val tvId = itemView.findViewById<TextView>(R.id.tvId)
    val tvArtistName = itemView.findViewById<TextView>(R.id.tvDescription)
    val tvTrackName = itemView.findViewById<TextView>(R.id.tvName)
    val btWishList = itemView.findViewById<ImageButton>(R.id.btWishList)
    var btWishListClickListener = {}

    init {
        btWishList.setOnClickListener { btWishListClickListener() }
    }
}