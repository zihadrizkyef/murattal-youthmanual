package com.zihadrizkyef.murattalyouthmanual.ui

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import com.facebook.AccessToken
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.zihadrizkyef.murattalyouthmanual.R


class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        Handler().postDelayed({
            checkLogin()
        },100)
    }

    override fun onBackPressed() {
        //rapopo
    }

    private fun checkLogin() {
        val account = GoogleSignIn.getLastSignedInAccount(this)
        val isGoogleLoggedIn = account != null
        val accessToken = AccessToken.getCurrentAccessToken()
        val isFbLoggedIn = accessToken != null && !accessToken.isExpired
        if (isGoogleLoggedIn || isFbLoggedIn) {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
            finish()
        } else {
            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
            finish()
        }
    }
}
