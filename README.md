# Murattal-Youthmanual

Apps to listen for quran recitation

## HOW TO BUILD
1.  Open the project with android studio
2.  Plug your android to your laptop with USB
3.  Run the project by clicking run button

    ![Run Button](https://gitlab.com/zihadrizkyef/murattal-youthmanual/raw/master/image/runbutton.png)
4.  Select your phone then click OK

    ![Select Phone](https://gitlab.com/zihadrizkyef/murattal-youthmanual/raw/master/image/selectphone.png)